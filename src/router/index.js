import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'register',
    component: () => import('../components/Register.vue'),
    meta: {
      guest: true,
    },
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../components/Login.vue'),
    meta: {
      guest: true,
    },
  },
  {
    path: '/verify',
    name: 'verifyEmail',
    component: () => import('../components/EmailVerify.vue'),
    meta: {
      guest: true,
    },
  },
  {
    path: '/dashboard',
    name: 'dashboardComp',
    component: () => import('../components/Dashboard.vue'),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/profile',
    name: 'profile',
    component: () => import('../components/Profile.vue'),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/*',
    name: 'PageNotFound',
    component: () => import('../components/PageNotFound.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (localStorage.getItem('jwt') == null) {
      next({
        path: '/login',
        params: { nextUrl: to.fullPath },
      });
    } else {
      next();
    }
  } else {
    if (localStorage.getItem('jwt') == null) {
      next();
    } else {
      next({ path: '/dashboard' });
    }
  }
});

export default router;
